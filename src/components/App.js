import React, { Component } from 'react';
import axios from 'axios';
import '../styles/App.css';
import Navigation from './Navigation';
import Footer from './Footer';
import Order from './Order';
import Item from './Item';

class App extends Component {
  state = {
    items: {},
    order: {},
    promos: {},
    shipping: {}
}

componentWillMount() {
  
  // We store non-submitted orders in user's LocalStodage. Checking if they exist.
  const localStorageRef = localStorage.getItem("ram");
  if(localStorageRef) {
      this.setState({ order: JSON.parse(localStorageRef)});
  };

  // When component mounts, we request inventory, promos and shipping options
  this.items = axios
  .get(`http://jst.edchavez.com/api/inventory/getInventory/`)
  .then(res => {
    const items = res.data.items;
    this.setState({
      items
    });
  });

  this.promos = axios
  .get(`http://jst.edchavez.com/api/promo/`)
  .then(res => {
    const promos = res.data;
    this.setState({
      promos
    });
  });

  this.shipping = axios
  .get(`http://jst.edchavez.com/api/shipping`)
  .then(res => {
    const shipping = res.data;
    this.setState({
      shipping
    });
  });

}

componentDidUpdate() { 
  localStorage.setItem("ram", JSON.stringify(this.state.order));
}

addToOrder = (key) => {
  const order = {...this.state.order};
  order[key] = order[key] +1 || 1;
  this.setState({ order });
}

deleteFromOrder = (key) => {
  const order = {...this.state.order};
  delete order[key];
  this.setState({ order })
}

  render() {
    return (
      <div className="container">
        <Navigation />
        <div className = "home">
          <div className="items">
          <h2>For Sale</h2>
            {Object.keys(this.state.items).map(key => <Item addToOrder={this.addToOrder} details={this.state.items[key]} key={key} index={key}>{key}</Item>)}
          </div>
          <div className="order"><Order items={this.state.items} promos={this.state.promos} order={this.state.order} deleteFromOrder={this.deleteFromOrder} /></div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default App;
