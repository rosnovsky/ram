import React from 'react';
import '../styles/App.css';

const Footer = () => (
      <div className="footer">
        <p className="light">
          © {new Date().getFullYear()} Jon Snow. All rights reserved.
        </p>
      </div>
)

export default Footer;

