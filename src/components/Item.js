import React, { Component } from 'react';
import { formatPrice } from "../helpers";
import '../styles/App.css';

export default class Item extends Component {
  render() {
    const { name, description, price, itemId, inStock } = this.props.details;
    const isAvailable = inStock;
    return (
      <React.Fragment>
        <div className="itemcard" key={itemId}>
          <h3>{name}</h3>
          <p> {description} </p>
          <div className="buy"><p className="price">{formatPrice(price)} </p><button disabled={!isAvailable} onClick={() => this.props.addToOrder(this.props.index)}>{isAvailable ? "Add to cart" : "Sold Out"}</button></div>
        </div>
      </React.Fragment>
    );
  }
}
