import React, { Component } from 'react';
import { formatPrice } from "../helpers";
import '../styles/App.css';

class Order extends Component {
    
    renderOrder = (key) => {
    const item = this.props.items[key];
    const count = this.props.order[key];
    const isAvailable = item && item.inStock === true;

    if(!item) {
      return;
    };

    if(!isAvailable){
      return (
        // in case an item is sold out while in customer's cart
        <li key className="order">Sorry, {item ? item.name : "this item"} is sold out ☹ <button onClick={() => this.props.deleteFromOrder(key)}>&times;</button></li>
    )}

    return (
      <li key={key}>    
        <span>
          <span className="subtotalPrice"><strong>{item.name}</strong> x {count}: {formatPrice(count * item.price)}</span>
          <button className="delete" onClick={() => this.props.deleteFromOrder(key)}><small>&times;</small></button>  
        </span>
      </li>
    )}

  render() {
    // this is a dirty hard-coded hack to make some promos work. Here we only check if a PercentOff typo of promo exists, and if so we'll apply it's off percentage to the order total. This is wrong and should be done very differently. We should check dates, if they apply, and only use promoId to further apply the discount, or something along this lines

    const promos = this.props.promos;
    let percentOff;

    for(let i = 0; i<this.props.promos.length; i++){
      if (promos[i].promotionType === "PercentOff"){
        percentOff = promos[i].promoAmt/100;
      }
    }

    const orderIds = Object.keys(this.props.order);
    const total = orderIds.reduce((prevTotal, key) => {
      const item = this.props.items[key];
      const count = this.props.order[key];
      const isAvailable = item && item.inStock === true;

      if(isAvailable) {
        return prevTotal + (count * item.price);
      }     
      return prevTotal;
    }, 0);

    return (
      <div className="order">
        <h2>Order</h2>
        <ul>
            {orderIds.map(this.renderOrder)}   
        </ul>
        
        <div className="total">
            Total: <strong>{total > 2000 ? formatPrice(total - total * percentOff) : formatPrice(total)}</strong>
        </div>
        {/* A Shipping options dropdown  */}
        {/* Place order button */}
      </div>
    )
  }
}

export default Order;
