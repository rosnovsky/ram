# RAM Mounts From End Hackathon 🔥

## Setup & Run 🛠

```bash
yarn install
yarn start
```

## Build 🗜

To deploy project, just do this:

```bash
yarn build
```

You'll have `build` folder available with all the stuff necessary for deployment to the server.

## Serve

To serve the production build right away, do this:

```bash
yarn global add serve
serve -s build
```